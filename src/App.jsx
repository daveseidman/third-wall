import React, { useRef, useEffect, useState } from 'react';
// import { Stats } from './assets/stats';
import {
  PerspectiveCamera, Scene, WebGLRenderer, Mesh, BoxGeometry, MeshNormalMaterial, GridHelper, Matrix4, Quaternion, Vector3,
} from 'three';
import { render } from 'react-dom';
import { AlvaAR } from './assets/alva_ar';
// import { ARSimpleView, ARSimpleMap } from './assets/view';
// import { Video, onFrame } from './assets/utils';
import sampleVideo from './assets/sample.mp4';
import Styles from './App.module.scss';

function App() {
  const map = useRef('map');
  const video = useRef('video');
  const videoCanvas = useRef('canvas');
  const renderCanvas = useRef('renderCanvas');

  const [alva, setAlva] = useState();
  const [videoContext, setVideoContext] = useState();
  const [cameraPose, setCameraPose] = useState();
  const [renderCamera, setRenderCamera] = useState();
  const [renderer, setRenderer] = useState();
  const [scene, setScene] = useState();
  const [testCube, setTestCube] = useState();

  useEffect(() => {
    // if (videoCanvas.current && !videoContext) {
    setVideoContext(videoCanvas.current.getContext('2d'));
    // }
  }, [videoCanvas]);

  useEffect(() => {
    // if(renderCanvas.current) {}
    const _scene = new Scene();
    const camera = new PerspectiveCamera(45, 720 / 1280, 0.1, 100);
    camera.position.z = -20;
    camera.position.y = 10;
    camera.lookAt(new Vector3());
    const _renderer = new WebGLRenderer({ canvas: renderCanvas.current });
    setRenderer(_renderer);
    setScene(_scene);
    const grid = new GridHelper(10, 10);
    const cube = new Mesh(new BoxGeometry(1, 1, 1), new MeshNormalMaterial());
    setTestCube(cube);
    _scene.add(grid);
    _scene.add(cube);
    setRenderCamera(camera);
  }, [renderCanvas]);

  useEffect(() => {
    if (!alva) {
      AlvaAR.Initialize(720, 1280).then((_alva) => {
        setAlva(_alva);
      });
    }
  }, []);

  useEffect(() => {
    // console.log(renderCamera);
    // renderCamera.applyMatrix(cameraPose);
    if (cameraPose) {
      const m = new Matrix4().fromArray(cameraPose);
      const r = new Quaternion().setFromRotationMatrix(m);
      const t = new Vector3(cameraPose[12], cameraPose[13], cameraPose[14]);
      // console.log(m, r, t);
      // renderCamera.position.set(t);
      // renderCamera.rotation.set(r);
      // testCube.position.set(t);
      testCube.position.set(t.x, t.y, t.z);
      testCube.quaternion.set(r.x, r.y, r.z, r.w);
      renderer.render(scene, renderCamera);
    }
    // ( rotationQuaternion !== null ) && rotationQuaternion.set( -r.x, r.y, r.z, r.w );
    // ( translationVector !== null ) && translationVector.set( t.x, -t.y, -t.z );
  }, [cameraPose]);

  const captureFrame = () => {
    videoContext.clearRect(0, 0, 720, 1080);
    videoContext.drawImage(video.current, 0, 0, 720, 1280);
    const data = videoContext.getImageData(0, 0, 720, 1280);
    const pose = alva.findCameraPose(data);
    if (pose) setCameraPose(pose);
    // console.log(pose);
    requestAnimationFrame(captureFrame);
  };
  return (
    <div className={Styles.main}>
      <h1>Third Wall Demo</h1>
      <video
        ref={video}
        autoPlay
        loop
        muted
        className={Styles.input}
        onCanPlay={captureFrame}
        width={720}
        height={1280}
      >
        <source src={sampleVideo} />
      </video>
      <canvas
        width={720}
        height={1280}
        ref={videoCanvas}
      />
      <canvas
        width={720}
        height={1280}
        ref={renderCanvas}
      />
      <div ref={map} />
    </div>
  );
}

export default App;
